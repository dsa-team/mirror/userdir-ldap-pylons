<% 

from creoleparser import creole2html
import re 

pstrip_re=re.compile(r'<p>(.*)</p>\n')

%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>db.debian.org -- Index</title>
  <link rel="stylesheet" href="http://www.debian.org/debian.css" type="text/css" />
</head>
<body>
 <div id="header">
  <div id="upperheader">
   <div id="logo">
      <a href="./" title="Debian Home"><img src="http://www.deb.at/Pics/openlogo-50.png" alt="Debian" width="50" height="61" /></a>
   </div> <!-- end logo -->
   <p class="section">db</p>
  </div> <!-- end upperheader -->

  <div id="navbar">
   <p class="hidecss"><a href="#inner">Skip Quicknav</a></p>
   <ul>
      <li><a href="http://www.debian.org/intro/about">About Debian</a></li>
      <li><a href="http://www.debian.org/distrib/">Getting Debian</a></li>
      <li><a href="http://www.debian.org/support">Support</a></li>
      <li><a href="http://www.debian.org/devel/">Developers'&nbsp;Corner</a></li>

   </ul>
  </div> <!-- end navbar -->
 </div> <!-- end header -->
 <div id="content">
  <div class="page_header">
   <p id="breadcrumbs"><a href="/">db.debian.org</a> / <a href="machines.cgi">Index</a> / Machine info: ${request.params['host']}.debian.org </p>
  </div>

  <h1>Server info for ${request.params['host']}.debian.org</h1>
  
  <table width=100%>
   % for key in ('access','admin','purpose'):
   <tr> 
    <td width="25%"><b>${key}:</b></td>
    % for counter in range(len(c.name[key])):
    <td>${pstrip_re.sub('\\1', creole2html(c.name[key][counter])) | n,trim}</td>
    % endfor
  </tr>
   % endfor
  </table>
  <hr>
  <table width=100%>
   % for key in ('machine','architecture','memory', 'distribution', 'disk', 'sshRSAHostKey'):
     <tr>
    <td width="25%"><b>${key}:</b></td>
     % for counter in range(len(c.name[key])):
     <td>${c.name[key][counter]}</td>
     % endfor
  </tr>
   % endfor
  </table>
  <hr>
  <table width=100%>
   <tr>
    <td width="25%"><b>sponsor:</b></td>
    % for counter in range(len(c.name['sponsor'])):
    <td>${pstrip_re.sub('\\1', creole2html(c.name['sponsor'][counter])) | n,trim}</td>
    % endfor
  </tr>
 </table>
</div>
</body>
</html>
