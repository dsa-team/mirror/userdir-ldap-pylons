<% 

from creoleparser import creole2html
import re 

pstrip_re=re.compile(r'<p>(.*)</p>\n')

%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>db.debian.org -- Index</title>
  <link rel="stylesheet" href="http://www.debian.org/debian.css" type="text/css" />
</head>
<body>
 <div id="header">
  <div id="upperheader">
   <div id="logo">
      <a href="./" title="Debian Home"><img src="http://www.deb.at/Pics/openlogo-50.png" alt="Debian" width="50" height="61" /></a>
   </div> <!-- end logo -->
   <p class="section">db</p>
  </div> <!-- end upperheader -->

  <div id="navbar">
   <p class="hidecss"><a href="#inner">Skip Quicknav</a></p>
   <ul>
      <li><a href="http://www.debian.org/intro/about">About Debian</a></li>
      <li><a href="http://www.debian.org/distrib/">Getting Debian</a></li>
      <li><a href="http://www.debian.org/support">Support</a></li>
      <li><a href="http://www.debian.org/devel/">Developers'&nbsp;Corner</a></li>

   </ul>
  </div> <!-- end navbar -->
 </div> <!-- end header -->
 <div id="content">
  <div class="page_header">
   <p id="breadcrumbs"><a href="/">db.debian.org</a> / <a href="machines.cgi">Index</a> </p>
  </div>

  Info about ${c.users}
  
  <table width=100%>
   <tr>
    <td>
% for user in enumerate(c.users):
 % for attribute in c.attrdict.keys():
   ${attribute[user]}
 % endfor

<%
#   % for key in ('uid','cn','sn','ircNick', 'jabberJID', 'keyFingerPrint'):
#     <tr>
#    <td width="25%"><b>${key}:</b></td>
#     % for counter in range(len(c.users[key])):
#     <td>${c.users[key][counter]}</td>
#     % endfor
#  </tr>
#   % endfor
%>
foo   
%endfor
    </td>
   </tr>
  </table>
 </div>
</body>
</html>
