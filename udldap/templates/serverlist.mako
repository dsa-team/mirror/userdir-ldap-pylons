## -*- coding: utf-8 -*-
<% 
import creoleparser
import re 

creole2html = creoleparser.Parser(dialect=creoleparser.create_dialect(creoleparser.creole10_base), method='html', encoding="ascii")
pstrip_re=re.compile(r'<p>(.*)</p>\n')

%>
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
 <head>
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
   <title>db.debian.org -- Index</title>
  <link rel="stylesheet" href="http://www.debian.org/debian.css" type="text/css" />
  <link rel="stylesheet" href="dbhome.css" type="type/css" />
  <script type="text/javascript" src="jquery-latest.min.js"></script> 
  <script type="text/javascript" src="jquery.tablesorter.min.js"></script> 
  <script type="text/javascript" id="js">$(document).ready(function() {
	// call the tablesorter plugin
	$("#servertable").tablesorter({
		sortList: [[0,0]],
		widgets: ['zebra']
	});

  }); </script>
 </head>
 <body>
  <div id="header">
   <div id="upperheader">
    <div id="logo">
       <a href="./" title="Debian Home"><img src="http://www.deb.at/Pics/openlogo-50.png" alt="Debian" width="50" height="61" /></a>
    </div> <!-- end logo -->
    <p class="section">db</p>
   </div> <!-- end upperheader -->
 
   <div id="navbar">
    <p class="hidecss"><a href="#inner">Skip Quicknav</a></p>
    <ul>
       <li><a href="http://www.debian.org/intro/about">About Debian</a></li>
       <li><a href="http://www.debian.org/distrib/">Getting Debian</a></li>
       <li><a href="http://www.debian.org/support">Support</a></li>
       <li><a href="http://www.debian.org/devel/">Developers'&nbsp;Corner</a></li>
 
    </ul>
   </div> <!-- end navbar -->
  </div> <!-- end header -->
  <div id="content">
   <div class="page_header">
    <p id="breadcrumbs"><a href="/">db.debian.org</a> / Index</p>
   </div>
 
   <h1>debian.org Developer Machines</h1>
   <div>
    <table id="servertable" border="1" class="tablesorter">
     <thead>
      <tr>
       % for attribute in c.attrlist:
       <th class="header">${attribute}</th>
       % endfor
      </tr>
     </thead>
     <tbody>
      % for i,host in enumerate(c.hosts):
       %if i%2:
      <tr>
       % else:
      <tr bgcolor='#EEEEEE'>
       % endif
       % for attribute in c.attrlist:
        % if attribute in ['host']:
       <td><a href="machines.cgi?host=${host[attribute][0]}">${host[attribute][0]}</a></td>
        % else:
         % if len(host[attribute]) == 1: 
       <td>${pstrip_re.sub('\\1', creole2html(host[attribute][0])) | n,trim}</td>
         % else:
       <td>
          % for item in host[attribute]:
          ${pstrip_re.sub('\\1', creole2html(item)) | n,trim}<br >
          % endfor
       </td>
         % endif
        % endif
       % endfor
      </tr>
      % endfor
     </tbody>
    </table>
   </div>
  </div>
 </body>
</html>
