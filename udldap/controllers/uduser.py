import logging
from userdir_objects import UDUser, udload_all, ATTR_DESC
from userdir_singletons import UDLdap, UDConf
from userdir_exceptions import UDError, UDLoadFail

from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect
from beaker.middleware import SessionMiddleware

from udldap.lib.base import BaseController, render

log = logging.getLogger(__name__)

class UduserController(BaseController):

    def __init__(self):
        self.l = UDLdap()
        self.o = UDUser()
        self.args = {}
        self.args['type'] = 'user'

        
    def index(self):
        # Check to see if a value is in the session
        if 'logged_in' in session:
            user = True
        else:
            user = False

        c.user = user
        return render('/usersearch.mako') 
    
    def search(self):
        search = {}
        c.users = list()
        if 'logged_in' in session:
            user = True
        else:
            user = False
            
        try:
            self.l.connect(self.args.get('BindUser', None), \
                  self.args.get('Password', None), True, True)
        except UDLoadFail, e:
            c.hosts = e
        
        # return str([self.args, request.params])
        for v in ['cn', 'sn', 'uid', 'ircnick', 'country']:
            if v in request.params:
                if request.params[v]: 
                    search[v] = "%s=%s" % (v, request.params[v])
                
        searchString=""
        for item in search:
            searchString = searchString + "(%s)" % search[item]
        searchString = "(&%s)" % searchString

        try: 
            objs = self.l.search_all(searchString, self.o._dn_base, ['dn'])
            logging.getLogger("root").info(objs)

                        
            for user in objs:
                logging.getLogger("root").info(str(user))
                # we strip uid= from the username
                self.args['name'] = user[0].split(",")[0][4:]
                logging.getLogger("root").info(str(self.args['name']))
                
                try: 
                    self.o.load(self.args['name'])
                except UDLoadFail, e:
                    c.hosts = e
                    logging.getLogger("root").warn(e)
                c.attrdict = self.o.attributes()
                uid = dict()
                
                for at in c.attrdict.keys():
                    if not at in ['modifiersName', 'createTimestamp', 'modifyTimestamp', 'host', 'hostname']:
                        if len(self.o.get(at)) == 0:
                            uid[at] = [u'']
                        else:
                            uid[at] = self.o.get(at)
                c.users.append(uid)
            return render('/userinfo.mako')
                
        except UDLoadFail, e:
                    c.hosts = e
        return str(c)
        # return render('/userlist.mako')
