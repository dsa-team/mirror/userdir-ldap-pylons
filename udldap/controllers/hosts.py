import logging
from userdir_objects import UDHost, udload_all, ATTR_DESC
from userdir_singletons import UDLdap, UDConf
from userdir_exceptions import UDError, UDLoadFail

from pylons import request, response, session, tmpl_context as c, url
from pylons.controllers.util import abort, redirect

from udldap.lib.base import BaseController, render

log = logging.getLogger(__name__)

class HostsController(BaseController):

    def __init__(self):
        self.l     = UDLdap()
        self.args = {} 
        self.args['type'] = 'host'

    def index(self):
        cfg = UDConf()
	
        c.hosts = list()

        try:
            self.l.connect(self.args.get('BindUser', None), \
                  self.args.get('Password', None), True, True)
        except UDLoadFail, e:
            c.hosts = e

        if 'host' not in request.params:
            objs = udload_all(UDHost)
            c.attrlist = [ 'host', 'architecture', 'sponsor', 'purpose', 'status', 'access' ]
            for obj in objs:
                host = dict()
                for at in c.attrlist:
                    if len(obj.get(at)) == 0:
                        host[at] = [u'']
                    else:
                        host[at] = obj.get(at)
                c.hosts.append(host)

            return render('serverlist.mako')

        self.args['name'] = request.params['host']
        obj = UDHost()
        try: 
            obj.load(self.args['name'])
        except UDLoadFail, e:
	    c.hosts = e

        attrdict = obj.attributes()
        #attrdict = [ 'access','admin','purpose','machine','architecture','memory','distribution','disk','sshRSAHostKey','sponsor' ]
        host = dict()
        for at in attrdict.keys():
            if not at in ['modifiersName', 'createTimestamp', 'modifyTimestamp', 'host', 'hostname']:
                if len(obj.get(at)) == 0:
                    host[at] = [u'']
                else:
                    host[at] = obj.get(at)
        c.name = host
        return render('/serverinfo.mako')


